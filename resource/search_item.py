# Sample dictionary
sample_dict = [
				{'name' : 'Tom', 'age' : 10},
				{'name' : 'Mark', 'age' : 5},
				{'name' : 'Pam', 'age' : 8},
				{'name' : 'Dicki', 'age' : 9}
			]
			
#-----------------------
# Method 1 : use filter
#-----------------------
search_val = filter(lambda person: person['name'] == 'Pam', sample_dict)
print search_val
# output : [{'age': 8, 'name': 'Pam'}]

#----------------------
# Methos 2: use next
#----------------------
search_val = next(item for item in sample_dict if item['name'] == 'Pam')
print search_val
# output : {'age': 8, 'name': 'Pam'}

search_val = next((item for item in sample_dict if item['name'] == 'Pam'), False)
print search_val
# output : {'age' : 8, 'name' : 'Pam'}

search_val = next((item for item in sample_dict if item['name'] == 'Sam'), False)
print search_val
# output : False

search_item = next(item for item in sample_dict if item['name'] == 'Mark')
print "Name : ", search_item['name']

# Referensi : http://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search