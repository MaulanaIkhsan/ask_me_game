sample_dict = [
				{'name' : 'Tom', 'age' : 10},
				{'name' : 'Mark', 'age' : 5},
				{'name' : 'Pam', 'age' : 8},
				{'name' : 'Dicki', 'age' : 9}
			]

for dct in sample_dict:
	if dct['name'] == 'Pam':
		dct['age'] = 20

last_item = sample_dict[-1]
print last_item['name']
