class Menu(object):
	def greetings_menu(self):
		print "\033[1;37;40m-----------------------------------------"
		print "|\t\033[1;36;40mWelcome in ask_me_game\t\t\033[1;37;40m|"
		print "-----------------------------------------\033[0;37;40m"

	def playing_menu(self):
		print "\033[1;37;40m-------------------------"
		print "|\t\033[1;36;40mLIST MENU\t\033[1;37;40m|"
		print "-------------------------"
		print "| [1] Take player\t|"
		print "| [2] Add player\t|"
		print "| [3] Check status\t|"
		print "| [4] Change player\t|"
		print "| [5] Delete player\t|"
		print "| [6] Show all player\t|"
		print "| [7] Restart status\t|"
		print "| [8] Exit\t\t|"
		print "-------------------------\033[0;37;40m"

	def add_player_menu(self):
		print "\033[1;37;40m---------------------------------"
		print "|\t\033[1;36;40mMENU TO ADD PLAYER\t\033[1;37;40m|"
		print "---------------------------------\033[1;37;40m"
		print "| [1] Add one player\t\t|"
		print "| [2] Add some player\t\t|"
		print "| [3] Cancel \t\t\t|"
		print "---------------------------------\033[0;37;40m"