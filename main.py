#from "nama file" import "nama class"
from players import Players
from menu import Menu
import os

# Call object menu
menus = Menu()

# Show welcome message
menus.greetings_menu()

total_player = raw_input(" Total player will be play : ")
users = Players(int(total_player))
suitable = users.check_suitable_seat()

while(suitable==False):
	print "\n\033[1;31;40m Please add total player, with maximum in 10 player and minimum in 3 player!\n\033[0;37;40m"
	total_player = raw_input(" Total player will be play : ")
	users = Players(int(total_player))
	suitable = users.check_suitable_seat()

print "\n"
users.add_some_players(int(total_player))

os.system("clear")
menus.playing_menu()
choose_menu = raw_input(" Choose : ")
choose_menu = int(choose_menu)

while (choose_menu != 8):
	# To take player and ask their
	if choose_menu == 1:
		if users.ready == 0 :
			print "\n"
			print "\033[1;31;40m All user already asked, please restart status all user\033[0;37;40m"
			print "\n"
			
			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)
		else:
			take_player_name = users.take_player_ask()
			print "\n"
			print " Target : \033[1;32;40m", take_player_name, "\033[0;37;40m"
			print "\n"
			users.ready -= 1
			users.already += 1
			users.change_player_status(take_player_name)

			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)

	# To add player
	if choose_menu == 2:
		print "\n"
		if users.available <= 0:
			print "\033[1;31;40m Sorry, you can't add player again because seat is full!\n\033[0;37;40m"
		else:
			menus.add_player_menu()
			choose_add_user = raw_input(" Choose : ")
			choose_add_user = int(choose_add_user)

			if choose_add_user == 1:
				print "\n"
				player_name = raw_input(" Player name : ")
				add_player = users.add_player(player_name)
				
				while (add_player == False):
					print "\033[1;31;40m\n Player name too long, please less than equal 10 character!\033[0;37;40m\n"
					player_name = raw_input(" Player name : ")
					add_player = users.add_player(player_name)

				print "\n\033[1;32;40m Congratulation, ", str(player_name), " success to added\033[0;37;40m\n"
				users.total_player += input_total_player
				menus.playing_menu()
				choose_menu = raw_input(" Choose : ")
				choose_menu = int(choose_menu)
			elif choose_add_user == 2:
				print "\n"
				input_total_player = raw_input(" Total player : ")
				input_total_player = int(input_total_player)

				if input_total_player > users.available:
					print "\n\033[1;31;40m Sorry, player has added is too many, please less than equal ", users.available," player!\n\033[0;37;40m"
					input_total_player = raw_input(" Total player : ")
					input_total_player = int(input_total_player)

					while (input_total_player > users.available):
						print "\n\033[1;31;40m Sorry, player has added is too many, please less than equal ", users.available," player!\n\033[0;37;40m"
						input_total_player = raw_input(" Total player : ")
						input_total_player = int(input_total_player)

					users.total_player += input_total_player
					users.add_some_players(input_total_player)
					
					print "\n\033[1;32;40m Congratulation, ", str(input_total_player), " success to added\033[0;37;40m\n"

					menus.playing_menu()
					choose_menu = raw_input(" Choose : ")
					choose_menu = int(choose_menu)

				else:
					users.total_player += input_total_player
					users.add_some_players(input_total_player)
					print "\n\033[1;32;40m Congratulation, ", str(input_total_player), " success to added\033[0;37;40m\n"
					
					menus.playing_menu()
					choose_menu = raw_input(" Choose : ")
					choose_menu = int(choose_menu)
			else :
				print "\n"
				menus.playing_menu()
				choose_menu = raw_input(" Choose : ")
				choose_menu = int(choose_menu)

	# To check status all players in seat
	if choose_menu == 3:
		print "\n"
		users.show_game_status()
		print "\n"
		menus.playing_menu()
		choose_menu = raw_input(" Choose : ")
		choose_menu = int(choose_menu)

	# Change player by name
	if choose_menu == 4:
		input_old_player_name = raw_input(" Old player name will changed : ")
		input_new_player_name = raw_input(" New player name will replacing : ")
		check_exist = users.check_player_exist(input_old_player_name)

		if (input_old_player_name == "0") or (input_new_player_name == "0"):
			print "\n\033[1;32;40m Cancel current operation success!\n\033[0;37;40m"
			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)
		else:
			while(check_exist == None):
				print "\n\033[1;31;40m Sorry, player with name '", input_old_player_name,"' not exist, please try again!\n\033[0;37;40m"
				input_old_player_name = raw_input(" Old player name will changed : ")
				input_new_player_name = raw_input(" New player name will replacing : ")
				check_exist = users.check_player_exist(input_old_player_name)

			change = users.change_player_name(input_old_player_name, input_new_player_name)

			if change == True:
				print "\n\033[1;32;40m Congratulation, ", input_old_player_name, " was updated\n\033[0;37;40m"
			else:
				print "\n\033[1;31;40m Sorry, occur fewer errors in system when editing player name!\n\033[0;37;40m"

			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)

	# Delete player by name
	if choose_menu == 5:
		input_player_name = raw_input(" Player name will delete : ")
		check_exist = users.check_player_exist(input_player_name)

		if input_player_name == "0":
			print "\n\033[1;32;40m Cancel current operation success!\n\033[0;37;40m"
			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)
		else:
			while (check_exist == None):
				print "\n\033[1;31;40m Sorry, player with name '", input_player_name,"' not exist, please try again!\n\033[0;37;40m"
				input_player_name = raw_input(" Player name : ")
				check_exist = users.check_player_exist(input_player_name)

			delete = users.delete_user(input_player_name)
			if delete == True:
				print "\n\033[1;32;40m Congratulation, ", input_player_name, " was deleted\n\033[0;37;40m"
			else:
				print "\n\033[1;31;40m Sorry, occur fewer errors in system when deleting player name player!\n\033[0;37;40m"

			menus.playing_menu()
			choose_menu = raw_input(" Choose : ")
			choose_menu = int(choose_menu)

	# Show all player and their status
	if choose_menu == 6:
		# print "Show all player"
		print "\n"
		users.show_all_player()
		print "\n"
		menus.playing_menu()
		choose_menu = raw_input(" Choose : ")
		choose_menu = int(choose_menu)

	# Restart all status players to "on"
	if choose_menu == 7:
		print "Restart status"
		menus.playing_menu()
		choose_menu = raw_input(" Choose : ")
		choose_menu = int(choose_menu)

print "\n\033[1;32;40m See you next time and have nice day\n\033[0;37;40m"