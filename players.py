import os
from random import randrange
import random

class Players(object):

	# This variable used to contain all players during this game be held
	seat = []
	
	# This variable used to contain maximum player can be play in this game 
	max_player = 10
	
	minimum_player = 3

	# This variable used to contain total player was inputed by an user 
	total_player = 0

	# This variable used to contain total player with status 'on'
	ready = 0

	# This variable used to contain total player with status 'off'
	already	= 0
	
	# This variable used to contain available total player can be add in seat
	available = max_player

	def __init__(self, total_player):
		self.total_player = total_player

	def check_total_player(self):
		if self.total_player > self.max_player:
			return False
		else:
			return True

	def check_total_character(self, name):
		if len(name) <= 10:
			return True
		else:
			return False

	def check_player_exist(self, name):
		search_player = next((player for player in self.seat if player['name'] == name), None)
		return search_player

	def check_suitable_seat(self):
		if (self.total_player <= self.max_player) and (self.total_player >= self.minimum_player):
			return True
		else:
			return False

	def get_index_seat(self, name):
		return next((index for (index, value) in enumerate(self.seat) if value['name'] == name), None)


	def add_player(self, name):
		check_character = self.check_total_character(name)

		if check_character == True:
			player = {}
			player['name'] = name
			player['status'] = 'On'
			self.seat.append(player)
			self.ready += 1
			self.available -= 1
		else:
			return False

	def add_some_players(self, total_input):
		for sequence in range(0, total_input):
			input_player_name = raw_input(" Player "+ str(sequence+1) +" name : ")
			put_player = self.add_player(input_player_name)

			while put_player == False:
				print "\033[1;31;40m\n Player name too long, please less than equal 10 character!\033[0;37;40m\n"
				
				input_player_name = raw_input(" Player "+ str(sequence+1) +" name : ")
				put_player = self.add_player(input_player_name)

	def show_all_player(self):
		print "\033[1;37;40m-----------------------------------------------------------------"
		print "| \t\t\t\033[1;36;40mLIST ALL PLAYERS \t\t\t\033[1;37;40m|"
		print "-----------------------------------------------------------------"
		print "|\tNo\t|\tPlayer Name\t|\tStatus\t\t|"
		print "-----------------------------------------------------------------"
		no = 1
		for player in self.seat:
			if len(player['name']) < 7:
				if player['status'] == 'On':
					print "|\t",no,"\t|\t",player['name'],"\t\t|\t\033[1;32;40m",player['status'],"\t\t\033[1;37;40m|"
				else:
					print "|\t",no,"\t|\t",player['name'],"\t\t|\t\033[1;31;40m",player['status'],"\t\t\033[1;37;40m|"
				no += 1
			else:
				if player['status'] == 'On':
					print "|\t",no,"\t|\t",player['name'],"\t|\t\033[1;32;40m",player['status'],"\t\t\033[1;37;40m|"
				else:
					print "|\t",no,"\t|\t",player['name'],"\t|\t\033[1;31;40m",player['status'],"\t\t\033[1;37;40m|"
				no += 1

		print "-----------------------------------------------------------------\033[0;37;40m"

	def delete_user(self, name):
		get_index = self.get_index_seat(name)
		
		if get_index == None:
			return False
		else:
			if self.seat[get_index]['status'] == 'On':
				self.ready -= 1
			else:
				self.already -=1

			self.available += 1
			del self.seat[get_index]

	def change_player_name(self, old_name, new_name):
		get_index = self.get_index_seat(old_name)

		if get_index == None:
			return False
		else:
			self.seat[get_index]['name'] = new_name

	def change_player_status(self, name):
		get_index = self.get_index_seat(name)

		if get_index == None:
			return False
		else:
			self.seat[get_index]['status'] = 'Off'

	def show_game_status(self):
		print "\033[1;37;40m---------------------------------"
		print "| \033[1;36;40m\tSTATUS OF GAME \t\t\033[1;37;40m|"
		print "---------------------------------"
		print "| Total Player\t | \t", self.total_player, "\t|"
		print "| Ready\t\t | \t", self.ready, "\t|"
		print "| Already\t | \t", self.already, "\t|"
		print "| Available\t | \t", self.available, "\t|"
		print "---------------------------------\033[0;37;40m"

	def restart_all_status(self):
		for player in self.seat:
			player['status'] = 'On'

	def take_player_ask(self):
		temp_seat = []
		# random.shuffle(self.seat)

		if self.already == 0:
			temp_seat = self.seat
			random_index = randrange(0, self.ready)
			random.shuffle(temp_seat)

			return temp_seat[random_index]['name']
		elif self.ready == 1:
			random.shuffle(self.seat)
			temp_seat = [player for player in self.seat if player['status'] == 'On']
			return temp_seat[0]['name']
		else:
			random.shuffle(self.seat)
			temp_seat = [player for player in self.seat if player['status'] == 'On']
			random_index = randrange(0, self.ready)
			random.shuffle(temp_seat)
			return temp_seat[random_index]['name']

